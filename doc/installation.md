## Installation

You can install Amethyst with pip:

```
pip install amethyst
```

You should now be able to use the `amethyst` command, and its shortcut `amy`.

If you have graphics troubles with this version, you can try the Qt version:

```
pip install amethyst[qt]
```

You may want to install the last, unstable version, built at each code update (not recommended):

```
pip install wheel https://roipoussiere.frama.io/amethyst/amethyst-latest.tar.gz
```

!!! Note
    If you or are not used to command line or don't know what is pip, just be patient: installation process will be improved in the future.

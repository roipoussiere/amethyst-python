amethyst(1) -- A general-purpose keyboard-based launcher.
=========================================================

## SYNOPSIS

`amy` [options] module name
`amethyst` [options] **--input** <manifest file> | <executable>

## DESCRIPTION

Amethyst is a general-purpose keyboard-based launcher that attempts to be both efficient and user-friendly.

It interacts with external programs to display a list of actions to the user, let him chose one, then execute it in order to do various tasks, from launching your favorite applications to configuring your monitors.

Amethyst displays a shape (called gem) composed of several choices (called faces), then executes an action accordingly to what has been selected.
Gems are described by a *manifest*, that gives information about it and its faces (labels, position, icon, etc.).

Amethyst is typically used with modules, that are executable files providing the gem manifest and actions to do.
<!-- Modules are stored in the modules folder (`~/.config/amethyst/modules` by default on Linux). -->

The `amy` command is a simple shortcut for `amethyst`: both can be used.

## EXAMPLES

Execute the `some_module` module with Amethyst:

    amethyst some_module

Load the `manifest.yml` manifest to Amethyst, then run the `./run.sh` executable, with the selected gem as first parameter:

    amethyst -i ./manifest.yml | ./run.sh

Load the manifest provided by the script `./input.sh `, then run the `./run.sh` executable, with the selected gem as first parameter:

    ./input.sh | amethyst -i - | ./run.sh

## OPTIONS

- `-i`, `--input` listing:
  Load the given gems list, then display the gems and wait for user choice, then print the result. The *listing* can either be a file path, an url, or `-` to load from stdin.
<!--
- `-f`, `--format` json|yaml|dmenu:
  Specify the format used by the gem list;

- `-p`, `--param` key=value:
  Set a parameter value for the module.

- `--test`:
  Test if the provided gem list is valid, then exit.

- `-l`, `--list`:
  List all available modules, then exit.

- `-a`, `--add` path [name]:
  Store the script located at *path* to Amethyst modules folder, then exit. If *name* is provided, amethyst will rename the file accordingly.

- `--cli`:
  Use a limited command-line interface instead of the Amethyst ui.

- `-d`, `--module-dir path:
  Look for the module in the given folder instead of the default module folder.

- `-c`, `--config` key1=value1,key2=value2[,...]:
  Provide a comma-separated list of configuration option, overriding the values from the configuration file.

- `-C`, `--config-file` path:
  Use the given configuration file instead of the default.

- `-s`, `--color-scheme` name:
  Use the given color scheme instead of the scheme defined in the configuration.

- `-S`, `--color-scheme-file` path:
  Use the given color scheme file instead of the scheme defined in the configuration.

- `-v`, `--version`:
  Print Amethyst version, then exit.
-->

- `-h`, `--help`:
  Print this help, then exit.

## AUTHOR

Nathanaël Jourdane <njourdane@protonmail.com>

## REPORTING BUGS

On Framagit: https://framagit.org/roipoussiere/amethyst/-/issues

## LICENSE

This project is licensed under the MIT license: https://mit-license.org/

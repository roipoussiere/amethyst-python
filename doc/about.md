# About Amethyst

## Author

Nathanaël Jourdane ([Mastodon](https://mastodon.tetaneutral.net/@roipoussiere), [Github](https://github.com/roipoussiere), [Framagit](https://framagit.org/roipoussiere))

## License

This project is licensed under the [MIT license](https://mit-license.org/).
